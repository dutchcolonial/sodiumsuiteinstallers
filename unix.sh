#!/bin/sh
# SodiumSuite Universal UNIX Installer

# Usage Examples:
# wget -q -O - https://gitlab.com/dutchcolonial/sodiumsuiteinstallers/raw/master/unix.sh | bash -s 123456
# curl -s https://gitlab.com/dutchcolonial/sodiumsuiteinstallers/raw/master/unix.sh | bash -s 123456
# fetch --no-verify-peer -o - https://gitlab.com/dutchcolonial/sodiumsuiteinstallers/raw/master/unix.sh | sh -s 123456
# cd /tmp && ftp https://gitlab.com/dutchcolonial/sodiumsuiteinstallers/raw/master/unix.sh && sh unix.sh 123456

# Set Variables Before Main

export companyID=$1
export masterURL="na1.waxquixotic.com"

if [ -z "$1" ]; then
  echo "No Company ID Provided, This is Required"
  exit 1
fi

################## Functions ########################

root_check() {
  if [[ $USER != "root" ]]; then 
    echo "This script must be run as root"
    exit 1 
  fi
}

## Linux Section  ********************************************

linux_common() {
  mkdir -p /etc/salt/
  echo $companyID > /etc/salt/companyName.txt 
  echo "master: $masterURL" > /etc/salt/minion 
  uuidgen > /etc/salt/minion_id 
  echo "45.76.23.0 salt" >> /etc/hosts
}

is_fedora() {
  root_check
  dnf -y install salt-minion 
  linux_common
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_rhel() {
  # Have to disable GPG Check for reliability reasons, real world issues with repos
  root_check
  yum -y install https://repo.saltstack.com/yum/redhat/salt-repo-latest-2.el7.noarch.rpm --nogpgcheck
  yum -y install python-util salt-minion --nogpgcheck
  linux_common 
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_deb() {
  root_check
  apt-get -y update 
  apt-get -y install python-psutil salt-minion uuid-runtime
  linux_common 
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_suse() {
  root_check
  zypper install -y salt-minion
  linux_common 
  systemctl restart salt-minion 
  systemctl enable salt-minion
  exit 
}

is_pacman() {
  root_check
  pacman -Sy salt
  linux_common
  systemctl disable salt-master
  systemctl stop salt-master
  systemctl restart salt-minion
  systemctl enable salt-minion 
  exit   
}

is_eopkg() {
  root_check
  linux_common
  cd /tmp
  wget https://github.com/saltstack/salt/archive/2018.3.zip
  unzip 2018.3.zip
  cd /tmp/salt-2018.3/
  python /root/salt-2018.3setup.py install
  cp /root/salt-2018.3/pkg/salt-minion.service /usr/lib/systemd/system/
  systemctl daemon-reload && systemctl enable salt-minion
  eopkg install python-pyzmq pycrypto python-msgpack python-tornado python-jinja pyyaml python-psutil 
  systemctl restart salt-minion
  exit 
}

is_pclinuxos() { 
  root_check
  apt-get -y update 
  apt-get -y install python-pyzmq python-pycrypto python-msgpack python-tornado python-pip gcc 
  apt-get -y python-pycrypto python-devel python-psutil
  pip install jinja jinja2 pyyaml 
  pip install salt
  linux_common
  salt-minion -d 
  echo "salt-minion -d" >> /etc/rc.d/rc.local 
  exit 
}

is_entropy() {
  root_check
  equo update
  equo install salt psutil 
  linux_common
  systemctl enable salt-minion
  systemctl restart salt-minion
  exit 
}

is_emerge() {
  root_check
  echo "Gentoo not supported." ; exit 
}

is_raspbian() {
  apt-get -y install uuid-runtime
  is_deb
  exit
}

is_alpine() {
  apk update
  apk add python curl py-psutil
  cd /tmp
  wget -O bootstrap-salt.sh https://bootstrap.saltstack.com
  sh bootstrap-salt.sh
  mkdir -p /etc/salt/
  echo $companyID > /etc/salt/companyName.txt 
  echo "master: $masterURL" > /etc/salt/minion  
  echo "45.76.23.0 salt" >> /etc/hosts
  curl https://www.uuidgenerator.net/ | grep uuid | cut -d '>' -f2 | cut -d '<' -f1 > /etc/salt/minion_id
  /etc/init.d/salt-minion stop; /etc/init.d/salt-minion start
  exit
}

is_kaos() {
  root_check
  pacman -Sy python2-pip python2-jinja pyzmq python2-m2crypto python2-psutil --noconfirm
  mkdir -p /etc/salt
  pip2 install --upgrade pip
  pip2 install salt
  linux_common
  salt-minion -d
}

is_unknown_linux() {
  root_check
  echo "Linux Type is Unknown, So Attempting a Brute Force"
  apt-get -y update && apt-get install salt-minion
  dnf -y install salt-minion
  yum -y install salt-minion
  pacman -Sy salt
  linux_common
  systemctl disable salt-master
  systemctl stop salt-master
  systemctl restart salt-minion
  systemctl enable salt-minion 
  echo "Resulting Install Status is Unknow:"
  echo "Please Report the Following to SodiumSuite Support..."
  echo OS is $OS
  echo VER is $VER
  exit   
}

## Windows Section *****************************************************

is_windows() {
  echo "Windows is not supported, use the Windows script"
  exit 1
}

## UNIX General Section ***********************************************

is_solaris() {
  cd /tmp
  curl -o 2018.3.zip https://codeload.github.com/saltstack/salt/zip/2018.3
  unzip 2018.3.zip
  cd /tmp/salt-2018.3/
  python setup.py install --force
  pkg install gcc pyyaml libtool autoconf automake pkg-config cmake
  wget -O /tmp/zmq.zip https://codeload.github.com/zeromq/libzmq/zip/master
  cd /tmp; unzip zmq.zip
  cd libzmq-master/
  ./autogen.sh 
  ./configure
  gmake
  gmake install
  ln -s /usr/include/gmp/gmp.h /usr/include/gmp.h
  pip install tornado jinja pyzmq pycrypto msgpack jinja2 psutil 
  mkdir -p /etc/salt; mkdir -p /opt/local/etc/salt 
  curl https://www.uuidgenerator.net/ | grep uuid | cut -d '>' -f2 | cut -d '<' -f1 > /opt/local/etc/salt/minion_id
  echo $companyID > /opt/local/etc/salt/companyName.txt 
  echo "master: $masterURL" > /opt/local/etc/salt/minion
  echo "45.76.23.0 salt" >> /etc/hosts
  salt-minion -d 
  exit 
}

## BSD Family Section ***************************************************

is_bsd() {
  #FreeBSD Tested
  env ASSUME_ALWAYS_YES=YES pkg bootstrap
  pkg install -y py27-salt
  pkg install -y py27-psutil
  echo $companyID > /usr/local/etc/salt/companyName.txt 
  echo "master: $masterURL" > /usr/local/etc/salt/minion 
  uuidgen > /usr/local/etc/salt/minion_id
  echo "45.76.23.0 salt" >> /etc/hosts 
  sysrc salt_minion_enable="YES"
  service salt_minion start
  if [ -f /usr/local/bin/pc-updatemanager ]; then
    echo "TrueOS: System Update Necessary"
    /usr/local/bin/pc-updatemanager pkgupdate
  fi
  exit
}

is_openbsd() {
  if ! [ -f "/etc/installurl" ]; then
    echo "https://ftp.openbsd.org/pub/OpenBSD/" >> /etc/installurl
  fi
  pkg_add salt
  pkg_add ossp-uuid py-psutil
  echo $companyID > /etc/salt/companyName.txt 
  echo "master: $masterURL" > /etc/salt/minion 
  uuid > /etc/salt/minion_id 
  echo "45.76.23.0 salt" >> /etc/hosts
  rcctl enable salt_minion
  rcctl restart salt_minion
  exit
}

is_netbsd() {
  if [ -z "$PKG_PATH" ]; then  
    export PKG_PATH="http://ftp.netbsd.org/pub/pkgsrc/packages/NetBSD/`uname -m`/`uname -r`/All/"
  fi
  pkg_add salt
  mkdir -p /etc/salt
  echo $companyID > /usr/pkg/etc/salt/companyName.txt
  cp /usr/pkg/etc/salt/companyName.txt /etc/salt/
  echo "master: $masterURL" > /usr/pkg/etc/salt/minion
  uuidgen > /usr/pkg/etc/salt/minion_id
  cp /usr/pkg/share/examples/rc.d/salt_minion /etc/rc.d/
  echo salt_minion="yes" >> /etc/rc.conf
  echo "45.76.23.0 salt" >> /etc/hosts
  /etc/rc.d/salt_minion start 
  exit 
}

is_dragonfly() {
  pkg update
  pkg install -y sysrc 
  pkg install -y py27-salt
  mkdir -p /etc/salt
  echo $companyID > /usr/local/etc/salt/companyName.txt 
  cp /usr/local/etc/salt/companyName.txt /etc/salt/
  echo "master: $masterURL" > /usr/local/etc/salt/minion 
  uuidgen > /usr/local/etc/salt/minion_id 
  echo "45.76.23.0 salt" >> /etc/hosts
  /usr/local/sbin/sysrc salt_minion_enable="YES"
  service salt_minion start
  exit
}

is_osx() {
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  brew install saltstack
  export PATH="/usr/local/Cellar/saltstack/2017.7.4/libexec/bin/:$PATH" 
  pip2 install psutil
  sudo mkdir -p /etc/salt
  mkdir -p /tmp/etc/salt
  echo $companyID > /tmp/etc/salt/companyName.txt
  echo "master: $masterURL" > /tmp/etc/salt/minion
  uuidgen > /tmp/etc/salt/minion_id
  sudo cp /tmp/etc/salt/* /etc/salt/
  su -c echo "45.76.23.0 salt" >> /etc/hosts
  sudo curl -L https://raw.githubusercontent.com/saltstack/salt/develop/pkg/darwin/com.saltstack.salt.minion.plist -o /Library/LaunchDaemons/com.saltstack.salt.minion.plist
  sudo launchctl load -w /Library/LaunchDaemons/com.saltstack.salt.minion.plist
  exit 
}

## Detection Section **************************************************************

detect_more() {
  OS=$(uname -s)
  case "$OS" in
    OpenBSD*) is_openbsd ;;
    NetBSD*)  is_netbsd ;; 
    *)        echo "Please tell SodiumSuite Support: $OSTYPE : $OS"  ;;
  esac
}

is_linux() {
  if [ -f /etc/os-release ]; then
    # freedesktop.org and systemd
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
  elif type lsb_release >/dev/null 2>&1; then
    # linuxbase.org
    OS=$(lsb_release -si)
    VER=$(lsb_release -sr)
  elif [ -f /etc/lsb-release ]; then
    # For some versions of Debian/Ubuntu without lsb_release command
    . /etc/lsb-release
    OS=$DISTRIB_ID
    VER=$DISTRIB_RELEASE
  elif [ -f /etc/debian_version ]; then
    # Older Debian/Ubuntu/etc.
    OS=Debian
    VER=$(cat /etc/debian_version)
  elif [ -f /etc/SuSe-release ]; then
    # Older SuSE/etc.
    OS=suse
  elif [ -f /etc/redhat-release ]; then
    # Older Red Hat, CentOS, etc.
    OS="CentOS Linux"
  elif [ -f /etc/alpine-release ]; then
    OS="Alpine Linux"
  else
    # Fall back to uname, e.g. "Linux <version>", also works for BSD, etc.
    OS=$(uname -s)
    VER=$(uname -r)
  fi

  if   [[ "$OS" == "Fedora" ]]; then
    is_fedora
  elif [[ "$OS" == "Ubuntu" ]]; then 
    is_deb
  elif [[ "$OS" == "CentOS Linux" ]]; then
    is_rhel
  elif [[ "$OS" == "Oracle Linux Server" ]]; then
    is_rhel 
  elif [[ "$OS" == "Linux Mint" ]]; then
    is_deb
  elif [[ "$OS" == "Deepin" ]]; then
    is_deb
  elif [[ "$OS" == "Debian GNU/Linux" ]]; then
    is_deb
  elif [[ "$OS" == "Sangoma Linux" ]]; then
    is_rhel
  elif [[ "$OS" == "openSUSE Leap" ]]; then
    is_suse
  elif [[ "$OS" == "openSUSE Tumbleweed" ]]; then
    is_suse
  elif [[ "$OS" == "elementary OS" ]]; then
    is_deb
  elif [[ "$OS" == "Korora" ]]; then
    is_fedora
  elif [[ "$OS" == "Zorin OS" ]]; then
    is_deb
  elif [[ "$OS" == "Antergos Linux" ]]; then
    is_pacman
  elif [[ "$OS" == "Manjaro Linux" ]]; then
    is_pacman
  elif [[ "$OS" == "Solus" ]]; then
    is_eopkg
  elif [[ "$OS" == "PCLinuxOS" ]]; then
    is_pclinuxos 
  elif [[ "$OS" == "Sabayon" ]]; then
    is_entropy
  elif [[ "$OS" == "Gentoo" ]]; then
    is_emerge
  elif [[ "$OS" == "KDE neon" ]]; then
    is_deb
  elif [[ "$OS" == "Netrunner" ]]; then
    is_deb
  elif [[ "$OS" == "Raspbian GNU/Linux" ]]; then
    is_raspbian
  elif [[ "$OS" == "Kali GNU/Linux" ]]; then
    is_deb
  elif [[ "$OS" == "Amazon Linux" ]]; then
    is_rhel
  elif [[ "$OS" == "Alpine Linux" ]]; then
    is_alpine
  elif [[ "$OS" == "ClearOS" ]]; then
    is_rhel
  elif [[ "$OS" == "KaOS" ]]; then
    is_kaos
  else
    echo "Unknown type, need a brute forcer. Diagnostics:"
    echo OS is $OS
    echo VER is $VER
  fi
}

check_ostype() {
  if [ -f /etc/alpine-release ]; then
    OSTYPE="linux" #Alpine Linux Doesn't List Linux
  fi

  case "$OSTYPE" in
    solaris*)   is_solaris ;;
    darwin*)    is_osx ;; 
    linux*)     is_linux ;;
    bsd*)       is_bsd ;;
    FreeBSD*)   is_bsd ;;
    DragonFly*) is_dragonfly ;;
    msys*)      is_windows ;;
    *)          detect_more ;;
  esac
}

main() {
  check_ostype
}

main "$@"
