# FoxRMM Windows PowerShell Installer 1.0
# 
# Written by Scott Alan Miller
# scott@ntg.co
#
# 3 Aug 2020


Write-Host "Installing FoxRMM Components..." ; Write-Host ""

# Set Higher TLS Level for Older PowerShell Instances
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

# Remove any old Salt Master keys, for safety
If ((Test-Path c:\salt\conf\pki\minion\minion_master.pub) -eq $True) {Rename-Item c:\salt\conf\pki\minion\minion_master.pub minion_master.pub.old}

# Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) 

# Install Salt Minion via Chocolatey
choco upgrade saltminion -y --ignore-checksums --force --force-dependencies 

# Configure the Salt Master, Overwrite whatever was there before
Set-Content -Path 'c:\salt\conf\minion.d\mast.conf' -Value 'master: na1.foxrmm.com'

# If no Minion ID has been created, create one now.
If ((Test-Path c:\salt\conf\minion_id) -eq $False) {Set-Content -Path c:\salt\conf\minion_id -Value ([guid]::NewGuid().toString())}
Else {Write-Host "Minion ID is Pre-existing"}
$minion_id = Get-Content c:\salt\conf\minion_id

# Restart the Salt Minion Process
net stop salt-minion
net start salt-minion

# Install Zabbix Agent via Chocolatey and configure with the Minion ID
net stop "Zabbix Agent"
choco uninstall zabbix-agent.install -y
choco install zabbix-agent.install -y
net stop "Zabbix Agent"

# Generate Zabbix Config
$zxconf = @"
LogType=file
LogFile=C:\Program Files\Zabbix Agent\zabbix_agentd.log
DenyKey=system.run[*]
Server=zx.foxrmm.com
ListenPort=10051
ServerActive=zx.foxrmm.com
Timeout=3
Hostname=$minion_id
"@
Set-Content -Path 'c:\Program Files\Zabbix Agent\zabbix_agentd.conf' -Value $zxconf
net start "Zabbix Agent"

Write-Host "Installation is now complete."

# Inform the user of the Minion ID for this host
Write-Host ""
Write-Host $minion_id

# Pass Info on Screen
Write-Host ""
Write-Host "If ID is incorrect, run this... "
Write-Host "Set-Content -Path c:\salt\conf\minion_id -Value ([guid]::NewGuid().toString())"
Write-Host ""

<#
  This script is idempotent so can be run safely at any time. It will remove the master key, but it will not reset any settings
  for the system or disrupt files other than correctly setting them. So if you run into issues, simply run again.

  Run command: 
  [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 ; Invoke-WebRequest -UseBasicParsing https://gitlab.com/dutchcolonial/sodiumsuiteinstallers/-/raw/master/fox.ps1 -OutFile fox.ps1 ; powershell -ExecutionPolicy ByPass ./fox.ps1

  If an old ID needs to be updated:
  Set-Content -Path c:\salt\conf\minion_id -Value ([guid]::NewGuid().toString())

#>
