choco install winlogbeat -y

# Generate WinLogBeat Config
$wlbconf = @"
winlogbeat.event_logs:
  - name: Application
    level: critical, error, warning
    ignore_older: 48h
  - name: Security
    level: critical, error, warning, information
    ignore_older: 48h
  - name: System
    level: critical, error, warning
    ignore_older: 48h
output.logstash:
 hosts: ["log.foxrmm.com:5044"]
logging.to_files: true
logging.files:
  path: C:\ProgramData\winlogbeat\Logs
logging.level: info
"@
Set-Content -Path 'c:\ProgramData\Chocolatey\lib\winlogbeat\tools\winlogbeat.yml' -Value $wlbconf
net start winlogbeat
